PORTS ?= -p 8080:80
IMAGE_NAME ?= registry.gitlab.com/amy-assistant/gate
CONTAINER_NAME ?= amy-gate

build: Dockerfile
	docker build -t $(IMAGE_NAME) .

push:
	docker push $(IMAGE_NAME)

run:
	docker run --rm -it --name $(CONTAINER_NAME) $(PORTS) $(IMAGE_NAME)

start:
	docker run -d --name $(CONTAINER_NAME) $(PORTS) $(VOLUMES) $(IMAGE_NAME)

stop:
	docker stop $(CONTAINER_NAME)

shell:
	docker exec -it $(CONTAINER_NAME) sh

rm:
	docker rm -f $(CONTAINER_NAME)